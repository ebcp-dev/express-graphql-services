# Express + GraphQL Microservices

Connect GraphQL instances through a central GraphQL API by Remote Stitching.

Followed the tutorial by David Novicki [GraphQL Microservices example with Remote Stitching using NodeJS and ExpressJS](https://codeburst.io/nodejs-graphql-micro-services-using-remote-stitching-7540030a0753)
